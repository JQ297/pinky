# pinky

Inky is an application to collect system information and data from the Pi-hole api and render it on an ePaper / eInk / EPD display.

![Pinky Example](pinky-example.png "Pinky Example")

## Hardware
**Required**
* A Raspberry Pi / Pi Zero / Pi Zero w with [Pi-hole](https://pi-hole.net/) installed. I used a Raspberry Pi Zero.
* An [Inky pHAT (ePaper/eInk/EPD) – Red/Black/White](https://shop.pimoroni.com/products/inky-phat)

**Possibly Required**
* If you have a Raspberry Pi Zero without wireless LAN connectivity: A micro USB to ethernet adapter. In any case a wired connection is recommended.

**Optional**
* The case in the picture is the [Pibow Zero W](https://shop.pimoroni.com/products/pibow-zero-w)

## Install Inky Python Libary
Follow the installation instructions on the [Getting Started with Inky pHAT](https://learn.pimoroni.com/tutorial/sandyj/getting-started-with-inky-phat) page to install the Ink Python library and to enable SPI and I2C.

## Setup
Make sure you are **in the root of your home folder** and then clone this repository:
~~~
git clone git@gitlab.com:JQ297/pinky.git
~~~

Start the Pinky script using:

~~~
cd ./pinky
python main.py
~~~

## Autostart on boot
Alternatively, run Pinky as a systemd service that automatically starts on boot:

~~~
cd ./pinky
sudo cp pinky.service /etc/systemd/system
sudo systemctl daemon-reload
sudo systemctl enable pinky.service
~~~

Reboot the system. Pinky will start running.

