import json
import subprocess
import time

import inky
import requests
from PIL import Image, ImageFont, ImageDraw
from font_fredoka_one import FredokaOne

class pinky:
    '''Pinky class

    This class collects data from the pihole api and renders it on a Inky pHAT display.
    '''

    def __init__(self):
        try: # Try to automatically detect the Inky pHAT.
            self.inky_display = inky.auto(ask_user=True, verbose=False)
        except TypeError:
            raise TypeError("You need to update the Inky library to >= v1.1.0")

        # Set font, font size and border color.
        self.font = ImageFont.truetype(FredokaOne, 22)
        self.inky_display.set_border(self.inky_display.RED)

        # Create new canvas to draw on.
        self.img = Image.new("P", (self.inky_display.WIDTH, self.inky_display.HEIGHT), 0)
        self.draw = ImageDraw.Draw(self.img)

        while True:
            try:
                self.clients, self.ads_blocked, self.ads_percentage, self.dns_queries = self.get_pihole_stats()
                self.ips = self.get_ip_address()
                self.hostname = self.get_hostname()
                self.time=self.get_current_time()

                succes = self.render(self)

                time.sleep(600) # Refresh display every 600 seconds.

            except Exception as e:
                print(e)

    @classmethod
    def get_pihole_stats(cls):
        '''Collect data from Pi Hole api and return: number of clients, amount of ads blocked, percentage of the total dns queries blocked  and total dns queries made.

        :return: number of unique clients, # of ads blocked today, % of ads blocked today, dns queries made today [array of strings]
        '''
        data = json.loads(requests.get("http://localhost/admin/api.php").text)
        return data['unique_clients'], data['ads_blocked_today'], data['ads_percentage_today'], data['dns_queries_today']

    @classmethod
    def get_current_time(cls):
        '''Return current time.

        :return: formatted HH:MM [string]
        '''
        return time.strftime("%H:%M", time.localtime())

    @classmethod
    def get_ip_address(cls):
        '''Get the ip address of the device.

        :return: ip address of the device. [string]
        '''
        return subprocess.check_output('hostname -I | cut -d" " -f1', shell=True).decode("utf-8").strip('\n')

    @classmethod
    def get_hostname(cls):
        '''Get the hostname of the device.

        :return: [device hostname].local [string]
        '''
        return subprocess.check_output('hostname', shell=True).decode("utf-8").strip('\n').lower()+'.local'

    @staticmethod
    def render(cls):
        '''Render a frame to the display.

        :return: true (success) / false (failure) [boolean]
        '''

        # Try to render the data to the screen.
        try:

            # Draw black background.
            for y in range(0, cls.inky_display.HEIGHT):
                for x in range(0, cls.inky_display.WIDTH):
                    cls.img.putpixel((x, y), cls.inky_display.BLACK)

            # Draw the text to the canvas.
            cls.draw.text( ((cls.inky_display.WIDTH/2)-(cls.font.getsize(str(cls.hostname))[0]/2), 0), str(cls.hostname), cls.inky_display.WHITE, cls.font)
            cls.draw.text( ((cls.inky_display.WIDTH/2)-(cls.font.getsize(str(cls.ips))[0]/2), 25), str(cls.ips), cls.inky_display.WHITE, cls.font)
            cls.draw.text( ((cls.inky_display.WIDTH/2)-(cls.font.getsize(str(cls.ads_blocked) + "/" + str(cls.dns_queries)+"~"+str("%.d" % round(cls.ads_percentage, 0)))[0]/2), 50), str(cls.ads_blocked) + "/" + str(cls.dns_queries)+"~"+str("%.d" % round(cls.ads_percentage, 0)), cls.inky_display.WHITE, cls.font)
            cls.draw.text( ((cls.inky_display.WIDTH/2)-(cls.font.getsize(str(cls.clients)+(" client", " clients")[cls.clients>1]+"@"+cls.time)[0]/2), 75), str(cls.clients)+(" client", " clients")[cls.clients>1]+" @ "+cls.time, cls.inky_display.WHITE, cls.font)

            # Set image to Inky's internal buffer and update the display.
            cls.inky_display.set_image(cls.img)
            cls.inky_display.show()

        except Exception as e:
            print(e)
            return False

        return True

if __name__ == '__main__':
    pinky()
